import mongoose from "mongoose";

const databaseConnection = async () => {
    //database connection
    try{
        const connection = await mongoose.connect(process.env.MONGO_URI, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
        })
        //database connect alert
        console.log('Database connected')
    } catch (error){
        console.log(`Error: ${error.message}`)
        process.exit(1)
    }
}

export default databaseConnection
