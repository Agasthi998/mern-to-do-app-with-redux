import mongoose from "mongoose";

const todoSchema = mongoose.Schema({
    text: {
        type: String,
        required: true
    },
    complete: {
        type: Boolean,
        default: false
    },
    timeStamp: {
        type: String,
        default: Date.now()
    }
})

const Todo = mongoose.model('todo', todoSchema)

export default Todo