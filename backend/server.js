import express from "express";
import databaseConnection from "./config/database.js";
import dotenv from "dotenv";

dotenv.config();
databaseConnection();

const app = express();

app.use(express.json());

app.get("/", (req, res) => {
  res.send("API is running...");
});

const PORT = process.env.PORT || 6500;

app.listen(PORT, console.log(`server running in port ${PORT}`));
